function AddrDto(address,priKey){
    this.address = address;
    this.priKey = priKey;
    return this;
}
function Wallet(name,address,encrypted,ph,balance){
    this.name = name;
    this.address = address;
    this.encrypted = encrypted;
    this.balance = balance;
    this.ph = ph;
    return this;
}
exports.AddrDto = AddrDto;
exports.Wallet = Wallet;