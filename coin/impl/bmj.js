let ad = require('../addrDto')

let utils = require('../utils');
const BigNumber = require('bignumber.js');
const axios = require('axios');

// 引入之后使用new创建全局chainsql对象，之后使用chainsql对象进行接口操作

//bmjsdk.connect("ws://121.40.110.130:15001");


function CoinClient(currConfig) {
    const bmjsdk = undefined;
    try{
        const BmjSDK = require('bmjsdk').BmjBackendAPI;
        bmjsdk = new BmjSDK();
    }catch(e){
        error(e)
    }
    // bmjsdk.connect(currConfig.rpcUrl);
    var t = this;
    /**
     * chainsql新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = function newWallet(id) {
        let addrDto = new ad.AddrDto(currConfig.account + ";" + id);
        addrDto.priKey = "none";
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address) {
        return true;
    }
    /**
     * bmj获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        let balance = await bmjsdk.getIssueCurrencyBalance();
        console.log("自定义 coin balance", balance);
        fn && fn(balance)
        return balance;
    }

    /**
     * 以太坊转账
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(from, to, amount, priKey, fn) {

        let transferRet = await bmjsdk.transferCurrency(to,'100');
        console.log('transferRet',transferRet);

    }
    this.collect = async function(from, to, priKey, fn) {

    }
    this.getBlockNumber = async function() {
        var accountLine = await t.getAccountLine(currConfig.account);
        return accountLine.result.ledger_index;
    }
    this.getAccountLine = async function(account) {
        var accountLine = null;
        await axios.post(currConfig.rpcUrl, {
                "method": "account_lines",
                "params": [{
                    "account": account,
                    "ledger_index": "validated"
                }]
            }).then(function(response) {
                accountLine = response.data;
                //console.log(response);
                log("发送请求获取accountLIne成功" + response.status)
            })
            .catch(function(error) {
                console.error("发送请求获取accountline 发生异常", error);
            });
        return accountLine;
    }
    this.getBlock = async function(blockNumber) {
        var block = null;
        await axios.post(currConfig.rpcUrl, {
                "method": "ledger",
                "params": [{
                    "ledger_index": blockNumber,
                    "accounts": false,
                    "full": false,
                    "transactions": true,
                    "expand": false,
                    "owner_funds": false
                }]
            }).then(function(response) {
                block = response.data;
                //console.log(response);
                log("发送请求获取block成功" + response.status)
            })
            .catch(function(error) {
                console.error("发送请求获取block发生异常", error);
            });
        return block;
    }
    this.getTx = async function(txid) {
        var tx = null;
        await axios.post(currConfig.rpcUrl, {
                "method": "tx",
                "params": [{
                    "transaction": txid
                }]
            }).then(function(response) {
                //console.log(response);
                tx = response.data;
                log("发送请求获取tx成功" + response.status)
            })
            .catch(function(error) {
                console.error("发送请求获取tx发生异常", error);
            });
        return tx;
    }

    this.syncBlockChainTx = async function(bcSync, fn) {

        var headBlockNumber = await t.getBlockNumber();
        while (bcSync.blockNumber <= headBlockNumber) {
            //查询交易信息
            var block = await t.getBlock(bcSync.blockNumber);
            if (!block || block.result.status != "success") {
                throw new Error("get block error!");
            }
            if (block && block.result.ledger.transactions) {
                for (var i = 0; i < block.result.ledger.transactions.length; i++) {
                    var txid = block.result.ledger.transactions[i];
                    var tx = await t.getTx(txid);
                    if (tx != null) {
                        if (tx.result.status != "success") {
                            throw new Error("get tx error!");
                        };
                        if(tx.result.Destination  == currConfig.account){
                            log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                        }
                        if (tx.result.Destination && tx.result.Destination == currConfig.account &&
                            tx.result.TransactionType && tx.result.TransactionType == "Payment" &&
                            tx.result.Amount.issuer &&
                            tx.result.Amount.currency && tx.result.Amount.currency == "BMJ") {
                            console.log("recv recharge, tx hash:" + tx.result.hash + " Tag:" + tx.result.SourceTag);
                            //转账金额
                            let amount = new BigNumber(tx.result.meta.delivered_amount.value);

                            await fn({
                                hash: txid,
                                index: -1,
                                blockHash: "",
                                blockNumber: tx.result.ledger_index,
                                fromAddr: tx.result.Account,
                                toAddr: tx.result.Destination + ";" + tx.result.SourceTag,
                                amount: amount
                            });
                        }
                    }
                }
                bcSync.blockNumber++;
                log("bmj block number " + bcSync.blockNumber)
            }
        }
    }
    return this;
}
exports.CoinClient = CoinClient;