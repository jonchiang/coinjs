let ad = require('../addrDto')
let bitcore = require('bitcore-lib');
let Unit = bitcore.Unit;
let RpcClient = require('bitcoind-rpc');
const BigNumber = require('bignumber.js');
rpcPort = '8332';
var t = this;

let CoinClient = function (currConfig) {
    var t = this;
    if (currConfig.rpcPort) {
        rpcPort = currConfig.rpcPort;
    }
    var rpcConfig = {
        protocol: 'http',
        user: currConfig.rpcUser,
        pass: currConfig.rpcPwd,
        host: currConfig.rpcHost,
        port: rpcPort,
    };
    var net = bitcore.Networks.mainnet;
    if (currConfig.env != "prod") {
        net = bitcore.Networks.testnet;
    }
    // config can also be an url, e.g.:
    // var config = 'http://user:pass@127.0.0.1:18332';

    var rpcClient = new RpcClient(rpcConfig);

    /**
     * 新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = async function newWallet() {
        var privateKey = new bitcore.PrivateKey();
        //log(privateKey.toWIF());
        let wif = privateKey.toWIF();
        let address = privateKey.toAddress(net).toString();
        let addrDto = new ad.AddrDto(address, wif);
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address){
        return await bitcore.Address.isValid(address,net);
    }
    /**
     * 获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        await rpcClient.listunspent(1, 99999, [address], function (error, parsedBuf) {
            //console.log(error,parsedBuf)
            if (parsedBuf && parsedBuf["result"]) {
                var balance = 0;
                let utxos = parsedBuf["result"];
                if (!utxos || utxos.length < 1) {
                    return;
                } else {
                    for (var i = 0; i < utxos.length; i++) {
                        let utxo = utxos[i];
                        balance += utxo.amount;
                    }
                }
                fn && fn(balance);
            }


        });

    }

    /**
     * 交易
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(data, privateKeyStr, fn) {
        log(data)
        let privateKey = new bitcore.PrivateKey(privateKeyStr);
        await rpcClient.listunspent(1, 99999, [data.from], function (error, parsedBuf) {
            //console.log(error,parsedBuf)
            if (parsedBuf && parsedBuf["result"]) {
                let utxos = parsedBuf["result"];
                if (!utxos || utxos.length < 1) {
                    alert("余额不足，无法完成转账!");
                    return;
                }
                let availableUtxos = new Array();
                var sumAmount = 0;
                var fee = 0.00005;
                for (var i = 0; i < utxos.length; i++) {

                    let utxo = utxos[i];
                    if (utxo.amount <= 0) {
                        continue;
                    }
                    sumAmount += utxo.amount;
                    availableUtxos.push(utxo);
                    if (sumAmount >= data.amount + fee) {
                        break;
                    }
                }
                log(availableUtxos)
                var tos = data.to.split(",");
                //多output 转账
                let transaction = new bitcore.Transaction().fee(Unit.fromBTC(fee).toSatoshis())
                    .from(availableUtxos);

                var amount = Unit.fromBTC(data.amount / tos.length).toSatoshis();
                tos.forEach(function (to, index) {
                    transaction = transaction.to(to, amount)
                })
                transaction = transaction.change(data.from).sign(privateKey);
                log(transaction);
                log(transaction.serialize())
                rpcClient.sendRawTransaction(transaction.serialize(), function (error, parsedBuf) {
                    console.log(error, parsedBuf)
                    fn && fn(parsedBuf["result"]);
                });
            }
        });
    }
    
     /**
     * 获取未花费输出详情
     */
    this.gettxout = async function gettxout(txid,index, fn) {
        return await waitResolve("gettxout",txid,index,fn);
    }
    this.collect = async function (utxos, to, priKeys,fn) {
        let pks = new Array();
        priKeys.forEach(pk => {
            pks.push(new bitcore.PrivateKey(pk));
        });
        let availableUtxos = new Array();
        var sumAmount = new BigNumber(0);
        var fee = 0.00005;//TODO 修改手续费，改成计算
        for (var i = 0; i < utxos.length; i++) {
            let utxo = utxos[i];
            if (utxo.amount <= 0) {
                continue;
            }
            sumAmount = sumAmount.plus(utxo.amount);
            availableUtxos.push({
                "txId": utxo.txid,
                "outputIndex": utxo.vout,
                "address": utxo.address,
                "script": utxo.scriptPubKey,
                "satoshis": Unit.fromBTC(utxo.amount).toSatoshis()
            });
        }
        log(availableUtxos)
        //log(availableUtxos)
        //多output 转账
        let transaction = new bitcore.Transaction().fee(Unit.fromBTC(fee).toSatoshis())
            .from(availableUtxos);
        var sumAmountBTC = sumAmount.minus(fee).toNumber();
        if(sumAmountBTC < 0.0001){
            log("btc过小，不予交易！");
            return;
        }
        transaction = transaction.to(to, Unit.fromBTC(sumAmountBTC).toSatoshis()).change(to);
        pks.forEach(pk => {
            transaction.sign(pk);
        })
        rpcClient.sendRawTransaction(transaction.serialize(), function (error, parsedBuf) {
            console.log(error, parsedBuf)
            if (parsedBuf && parsedBuf["result"]) {
               fn && fn(sumAmountBTC,fee,parsedBuf["result"]);
            }
        });

    }
    
    async function waitResolve(methodName, ...args) {
        var result = undefined;
        var promise = new Promise((resolve, reject) => {
            var callback = function (error, parsedBuf) {
                if (error) {
                    console.error(methodName, error);
                }
                if (parsedBuf && parsedBuf["result"]) {
                    result = parsedBuf["result"]
                }
                resolve();
            }
            rpcClient[methodName](...args, callback);
        });
        await promise;
        return result;
    }
    this.getBlockNumber = async function () {
        return await waitResolve("getblockcount");
    }
    this.getBlock = async function (blockNumber) {
        var hash = await waitResolve("getblockhash", blockNumber);
        return await waitResolve("getblock", hash);
    }
    this.getTx = async function (txid) {
        const tx = await waitResolve("getrawtransaction", txid, 1);
        //const tx = await waitResolve("decoderawtransaction", txHex);
        return tx;
    }
    this.getTxFromBlock = async function (blockNumberOrHash, index) {
        return await rpcClient.getTransaction(blockNumberOrHash, index);
    }
    this.syncBlockChainTx = async function (bcSync, fn) {

        const headBlockNumber = await t.getBlockNumber();
        log("btc head blockNumber is _ " + headBlockNumber);
        var tx = undefined;
        while (bcSync.blockNumber <= headBlockNumber) {

            //查询交易信息
            var block = await t.getBlock(bcSync.blockNumber);
            if (block && block.tx) {
                for (var i = 0; i < block.tx.length; i++) {
                    var bigTx = await t.getTx(block.tx[i]);
                    if (bigTx != null) {
                        for (var j = 0; j < bigTx.vout.length; j++) {
                            var tx = bigTx.vout[j];
                            if (tx.scriptPubKey.addresses) {
                                var toAddress = tx.scriptPubKey.addresses[0];
                                //log(tx.scriptPubKey.addresses)
                                await fn({ hash: bigTx.txid, index: tx.n, scriptPubKey: tx.scriptPubKey, blockHash: block.hash, blockNumber: block.height, fromAddr: "-", toAddr: toAddress, amount: tx.value });
                            }

                        }
                    }

                }


            }
            bcSync.blockNumber++;
            log("btc block number " + bcSync.blockNumber)
        }
    }
    return this;
}
exports.CoinClient = CoinClient;