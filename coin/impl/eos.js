let CoinClient = function (currConfig) {
    const ecc = require('eosjs-ecc')
    let ad = require('../addrDto')
    let utils = require('../utils')
    const { Api, JsonRpc } = require('eosjs');
    const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig'); // development only
    const signatureProvider = new JsSignatureProvider(["5Hww2xmCFnDN1rHzjZxb3AAPrgsp6LWJYACzqMTKU1AEq6mR9Ru"]); // node only; not needed in browsers
    const fetch = require('node-fetch');
    const rpc = new JsonRpc(currConfig.rpcUrl, { fetch });
    const { TextEncoder, TextDecoder } = require('util');
    const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });


 
    /**
     * 新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = async function newWallet(userId) {
        let addrDto = new ad.AddrDto(currConfig.account + ";" + utils.random10());
        addrDto.priKey = "none";
        log(addrDto)
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address){
        return true;
    }
    /**
     * 获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        let balanceEx = await rpc.get_currency_balance("eosio.token", address, "EOS");
        log(balanceEx)
        fn && fn(balanceEx[0])
        return balanceEx[0];
    }

    /**
     * 交易
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(data, privateKeyStr, fn) {
        let transferStore = JSON.parse(privateKeyStr)
        let transferSign = new JsSignatureProvider.default([transferStore.activePri]);
        var addresses = data.to.split(";");
        data.to = addresses[0];
        let transferApi = new Api({
            rpc: rpc,
            signatureProvider: transferSign,
            textDecoder: api.textDecoder,
            textEncoder: api.textEncoder
        });
        const result = await transferApi.transact({
            actions: [{
                account: 'eosio.token',
                name: 'transfer',
                authorization: [{
                    actor: data.from,
                    permission: 'active',
                }],
                data: {
                    from: data.from,
                    to: data.to,
                    quantity: (data.amount).toFixed(4) + ' EOS',
                    memo: 'transfer for sea',
                },
            }]
        }, {
                blocksBehind: 3,
                expireSeconds: 30,
            });
        log(result);
        fn && fn(result["transaction_id"]);
    }
    this.syncBlockChainTx = async function (bcSync, fn) {
        var actions = [];
        while (true) {
            console.log("query action form chain ",bcSync.account, bcSync.blockNumber, 13)
            await rpc.history_get_actions(bcSync.account, bcSync.blockNumber, 13).then(async function (data) {
                actions = data.actions;
                for (var i = 0; i < actions.length; i++) {
                    var action = actions[i].action_trace;
                    if (action.act["name"] == "transfer") {
                        var amounts = action.act.data.quantity.split(" ");
                        var memo = action.act.data.memo;
                        log("eos 转账请求" + amounts[0] + "_" + memo);
                        if (amounts[1] == "EOS") {//暂时先不支持其他代币
                            await fn({
                                hash: action.trx_id, index: 1, blockHash: action.block_num, blockNumber: action.block_num,
                                fromAddr: action.act.data.from, toAddr: action.act.data.to + ";" + memo, amount: amounts[0], symbol: amounts[1],
                                memo: action.act.data.memo
                            });
                        }
                    }
                }
            });
            bcSync.blockNumber += actions.length;
            if (actions.length < 13) {
                break;
            }
        }
    }
    return this;
}


exports.CoinClient = CoinClient;