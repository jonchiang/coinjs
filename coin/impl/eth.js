function CoinClient(currConfig) {
    let Web3 = require('web3');
    let ad = require('../addrDto')
    let Tx = require("ethereumjs-tx").Transaction;
    const BigNumber = require('bignumber.js');
    var web3 = new Web3(currConfig.rpcUrl);
    var network = "mainnet";
    if (currConfig.env != "prod") {
        network = "rinkeby";
    }
    var t = this;
    /**
     * 以太坊新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = function newWallet() {
        var addr = web3.eth.accounts.create();
        var addrDto = new ad.AddrDto(addr.address, addr.privateKey);
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address) {
        return await web3.utils.isAddress(address);
    }
    /**
     * 以太坊获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        var balance = await web3.eth.getBalance(address);
        balance = web3.utils.fromWei(balance);
        fn && fn(balance)
        return balance;
    }

    /**
     * 以太坊转账
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(from, to, amount, priKey, fn) {

        let privateKey = Buffer.from(priKey.replace("0x", ""), 'hex');
        let nonce = await web3.eth.getTransactionCount(from);

        let gasPrice = await web3.eth.getGasPrice();
        let fee =  web3.utils.fromWei((gasPrice * 22000 * 1.2)+"");
        let realAmount = new BigNumber(amount).minus(fee);

        console.log("以太坊转账 ", from, to, realAmount.toString())
        var wei = new BigNumber(web3.utils.toWei(realAmount.toNumber() + ""));
        const rawTx = {
            nonce: nonce,
            gasPrice: web3.utils.toHex(gasPrice*1.2),
            gasLimit: web3.utils.toHex(22000),
            to: to,
            value: "0x" + wei.toString(16),
            data: 'M',
        }
        let tx = new Tx(rawTx, { chain: network, hardfork: 'petersburg' });
        tx.sign(privateKey);
        let serializedTx = tx.serialize();
        web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(e, r) {
            console.log("r",r,e)
        }).on('receipt', function(r) {
            console.log("r",r)
            fn && fn(r, amount);
        });
    }
    this.collect = async function(from, to, priKey, fn) {
        let balance = await web3.eth.getBalance(from);
        t.transfer(from, to, web3.utils.fromWei(balance), priKey, fn)
    }
    this.getBlockNumber = async function() {
        return web3.eth.getBlockNumber();
    }
    this.getBlock = async function(blockNumber) {
        return web3.eth.getBlock(blockNumber);
    }
    this.getTx = async function(txid) {
        return web3.eth.getTransaction(txid);
    }
    this.getTxFromBlock = async function(blockNumberOrHash, index) {
        return web3.eth.getTransactionFromBlock(blockNumberOrHash, index);
    }
    this.estimateTokenFee = async function() {
        var gasPrice = await web3.eth.getGasPrice();
        // 40000 是 gas 消耗
        let fee = gasPrice * 60000 * 1.2;
        return web3.utils.fromWei(fee + "");
    }
    this.estimateFee = async function() {
        var gasPrice = await web3.eth.getGasPrice();
        let fee = gasPrice * 22000 * 1.2;
        return web3.utils.fromWei(fee + "");
    }
    this.estimateCollectFee = async function() {
        var gasPrice = await web3.eth.getGasPrice();
        //转eth 需要的手续费
        let ethFee = gasPrice * 22000 * 1.2; //1.2 是浮动，担心计算后 price变高导致无法交易
        //转eth token 需要的手续费
        let tokenFee = gasPrice * 60000 * 1.2;
        return web3.utils.fromWei((ethFee + tokenFee) + "");
    }
    this.getTxReceipt = async function(hash) {
        return web3.eth.getTransactionReceipt(hash);
    }
    this.syncBlockChainTx = async function(bcSync, fn) {
        const headBlockNumber = await t.getBlockNumber();
        var index = 0;
        var tx = undefined;
        do {
            //查询交易信息
            tx = await t.getTxFromBlock(bcSync.blockNumber, index);
            //log(bcSync.blockNumber + " 调用以太坊数据 ___ " + index)
            index++;
            if (tx != null) {
                t.applyTx(tx,fn);
            } else {
                index = 0;
                log(bcSync.blockNumber + " 调用以太坊数据 ___ ")
                bcSync.blockNumber++;
            }

        } while (bcSync.blockNumber <= headBlockNumber);
        if (bcSync.blockNumber > headBlockNumber) {
            bcSync.blockNumber = headBlockNumber + 1;
        }
    }

    this.applyTx = async function applyTx(tx,fn) {
        //转账金额是0 input长度大于2 基本可以判断是erc20转账
        if (tx.value == 0 && tx.input.length > 2) {
            if (tx.data == "M") {
                log("_______________________________________________________________________________收到矿工费：" + tx.data)
                //矿工费
            } else {
                var txReceipt = await t.getTxReceipt(tx.hash);
                if (txReceipt != null) {
                    if (txReceipt.logs[0] && txReceipt.logs[0].topics && txReceipt.logs[0].topics.length == 3 && txReceipt.logs[0].data.length) {
                        var amount = new BigNumber(web3.utils.toBN(txReceipt.logs[0].data));
                        var from = txReceipt.logs[0].topics[1].replace("0x000000000000000000000000", "0x");
                        var to = txReceipt.logs[0].topics[2].replace("0x000000000000000000000000", "0x");
                        var contractAddress = txReceipt.logs[0].address;
                        //TODO 修改DCID为当前代币ID
                        if (global.tokenMap && global.tokenMap[contractAddress]) {
                            var coin = global.tokenMap[contractAddress];
                            amount = amount.div(coin.precision);
                            var dcId = coin.id;
                            log(to + " 收到代币 " + contractAddress + ":" + amount)
                            fn({ hash: tx.hash, index: -1, dcId: dcId, blockHash: tx.blockHash, blockNumber: tx.blockNumber, fromAddr: from, toAddr: to, amount: amount });
                        }
                    }
                }
            }
        } else if (tx.value > 0) { //大于0的才算eth转账（其他的还有诸如创建合约 等等，不记录在内)
            fn({ hash: tx.hash, index: -1, blockHash: tx.blockHash, blockNumber: tx.blockNumber, fromAddr: tx.from, toAddr: tx.to, amount: web3.utils.fromWei(tx.value) });
        }
    }
    return this;
}
exports.CoinClient = CoinClient;