function CoinClient(currConfig) {
    let Web3 = require('web3');
    let ad = require('../addrDto');
    let Tx = require("ethereumjs-tx").Transaction;
    let t = this;
    const BigNumber = require('bignumber.js');
    var web3 = new Web3(currConfig.rpcUrl);
    log("token address:" + currConfig.contractAddress)
    let contract = new web3.eth.Contract(JSON.parse(currConfig.abi), currConfig.contractAddress);
    var precision = Math.pow(10, currConfig.decimals); //默认8位精度
    console.log(currConfig.decimals, precision)
    var network = "mainnet";
    if (currConfig.env != "prod") {
        network = "rinkeby";
    }
    /**
     * 以太坊新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = function newWallet() {
        var addr = web3.eth.accounts.create();
        var addrDto = new ad.AddrDto(addr.address, addr.privateKey);
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address) {
        return await web3.utils.isAddress(address);
    }
    this.estimateTokenFee = async function() {
        var gasPrice = await web3.eth.getGasPrice();
        // 40000 是 gas 消耗
        let fee = gasPrice * 60000 * 1.2;
        return web3.utils.fromWei(fee + "");
    }
    /**
     * 以太坊获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        var balance = await contract.methods.balanceOf(address).call();
        balance = new BigNumber(balance).dividedBy(precision)
        fn && fn(web3.utils.fromWei(balance))
        return balance;
    }
    this.collect = async function(from, to, priKey, fn) {


        var balance = await contract.methods.balanceOf(from).call();
        if (balance > 0) {
            var weiBalance = await web3.eth.getBalance(from);
            let ethBalance = web3.utils.fromWei(weiBalance);
            let ethTokenFee = await t.estimateTokenFee();
            log(from + " need:" + ethTokenFee + ":balance :" + ethBalance + ":" + balance)
            if (ethBalance < ethTokenFee) {
                console.log("没有足够的矿工费！" + from + ":to" + to + " has " + ethBalance + " need:" + ethTokenFee);
                fn && fn("-24");
                return;
            }
            t.transfer(from, to, balance, priKey, fn)
        }
    }
    /**
     * 以太坊转账
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(from, to, amount, priKey, fn) {
        let privateKey = Buffer.from(priKey.replace("0x", ""), 'hex');
        let nonce = await web3.eth.getTransactionCount(from);
        let gasPrice = await web3.eth.getGasPrice();
        //var amount = new BigNumber(amount).multipliedBy(precision)
        var cdata = contract.methods.transfer(to, amount).encodeABI();
        var rawTx = {
            "from": from,
            "nonce": nonce,
            "gasPrice": web3.utils.toHex(gasPrice * 1.2),
            "gasLimit": web3.utils.toHex(60000),
            "to": currConfig.contractAddress,
            "value": "0x0",
            "data": cdata,
            //"chainId": 0x03
        };

        let tx = new Tx(rawTx, { chain: network, hardfork: 'petersburg' });
        tx.sign(privateKey);
        let serializedTx = tx.serialize();
        web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(e, r) {}).on('receipt', function(r) {
            log("啊啊******************************************************啊啊啊啊")
            log(r)
            fn && fn(r, new BigNumber(amount).dividedBy(precision), 0);
        });
    }
    return this;
}
exports.CoinClient = CoinClient;