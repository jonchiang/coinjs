let CoinClient = function(currConfig) {
    let ad = require('../addrDto')
    const bitcoin = require('bitcoinjs-lib');
    let litecore = require('litecore-lib');
    let Unit = litecore.Unit;
    const networks = require('../networks')
    const networkId = networks.litecoin
    let RpcClient = require('bitcoind-rpc');
    var rpcConfig = {
        protocol: 'http',
        user: currConfig.rpcUser,
        pass: currConfig.rpcPwd,
        host: currConfig.rpcHost,
        port: '8336',
    };
    // config can also be an url, e.g.:
    // var config = 'http://user:pass@127.0.0.1:18332';

    var rpcClient = new RpcClient(rpcConfig);

    /**
     * 新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = async function newWallet() {
        //log(privateKey.toWIF());
        const keyPair = bitcoin.ECPair.makeRandom({ network: networkId })
        const wif = keyPair.toWIF()
        const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey, network: networkId })
        let addrDto = new ad.AddrDto(address, wif);
        return addrDto;
    }

    this.checkAddress = async function checkAddress(address){
        return await bitcore.Address.isValid(address,net);
    }
    /**
     * 获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        await rpcClient.listunspent(1, 99999, [address], function(error, parsedBuf) {
            //log(error,parsedBuf)
            if (parsedBuf && parsedBuf["result"]) {
                var balance = 0;
                let utxos = parsedBuf["result"];
                if (!utxos || utxos.length < 1) {
                    return;
                } else {
                    for (var i = 0; i < utxos.length; i++) {
                        let utxo = utxos[i];
                        balance += utxo.amount;
                    }
                }
                fn && fn(balance);
            }
        });

    }

    /**
     * 交易
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.transfer = async function transfer(data, privateKeyStr, fn) {
        let privateKey = new litecore.PrivateKey(privateKeyStr);
        await rpcClient.listunspent(1, 99999, [data.from], function(error, parsedBuf) {
            //log(error,parsedBuf)
            if (parsedBuf && parsedBuf["result"]) {
                let utxos = parsedBuf["result"];
                if (!utxos || utxos.length < 1) {
                    log("余额不足，无法完成转账!");
                    return;
                }
                let availableUtxos = new Array();
                var sumAmount = 0;
                var fee = 0.00005;
                for (var i = 0; i < utxos.length; i++) {

                    let utxo = utxos[i];
                    if (utxo.amount <= 0) {
                        continue;
                    }
                    sumAmount += utxo.amount;
                    availableUtxos.push(utxo);
                    if (sumAmount >= data.amount + fee) {
                        break;
                    }
                }
                log(availableUtxos)
                let transaction = new litecore.Transaction().fee(Unit.fromBTC(fee).toSatoshis())
                    .from(availableUtxos)
                    .to(data.to, Unit.fromBTC(data.amount).toSatoshis())
                    .change(data.from)
                    .sign(privateKey);
                log(transaction.serialize())
                rpcClient.sendRawTransaction(transaction.serialize(), function(error, parsedBuf) {
                    log(error, parsedBuf)
                    fn && fn(parsedBuf["result"]);
                });
            }
        });
    }
    return this;
}
exports.CoinClient = CoinClient;