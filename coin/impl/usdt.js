const BigNumber = require('bignumber.js');
let ad = require('../addrDto')
let bitcore = require('bitcore-lib');
let Unit = bitcore.Unit;
let RpcClient = require('omni-rpc');
let CoinClient = function (currConfig) {
    var t = this;
    var rpcConfig = {
        protocol: 'http',
        user: currConfig.rpcUser,
        pass: currConfig.rpcPwd,
        host: currConfig.rpcHost,
        port: '8432',
    };
    var net = bitcore.Networks.mainnet;
    var propertyId = 31;
    if (currConfig.env == "test") {
        net = bitcore.Networks.testnet;
        propertyId = 2147484960;
    }
    // config can also be an url, e.g.:
    // var config = 'http://user:pass@127.0.0.1:18332';

    var rpcClient = new RpcClient(rpcConfig);
    /**
     * 新建钱包
     * @returns {*|AddrDto}
     */
    this.newWallet = async function newWallet() {
        var privateKey = new bitcore.PrivateKey();
        //console.log(privateKey.toWIF());
        let wif = privateKey.toWIF();
        let address = privateKey.toAddress(net).toString();
        let addrDto = new ad.AddrDto(address, wif);
        return addrDto;
    }
    this.checkAddress = async function checkAddress(address){
        return await bitcore.Address.isValid(address,net);
    }
    /**
     * 获取余额
     * @param address
     * @param fn
     * @returns {Promise<*>}
     */
    this.getBalance = async function getBalance(address, fn) {
        await rpcClient.omni_getbalance(address, propertyId, function (error, parsedBuf) {
            log(error, parsedBuf)
            if (parsedBuf && parsedBuf["result"] && parsedBuf["result"]["balance"]) {
                fn && fn(parsedBuf["result"]["balance"])
            } else {
            }
        });
        return null;
    }
    /**
     * 获取未花费输出详情
     */
    this.gettxout = async function gettxout(txid,index, fn) {
        return await waitResolve("gettxout",txid,index,fn);
    }

    /**
     * 交易
     * @param data
     * @param privateKeyStr
     * @param fn
     * @returns {Promise<void>}
     */
    this.collect = async function collect(utxos, from, to, priKeys, fn) {
        if (utxos.length < 1) {
            error("USDT归集遇到错误,uxtos不能为空！");
            return;
        }
        var balanceRes = await waitResolve("omni_getbalance", from, propertyId);
        var amount = balanceRes.balance;
        let pks = new Array();
        priKeys.forEach(pk => {
            pks.push(new bitcore.PrivateKey(pk));
        });

        let availableUtxos = new Array();
        var sumAmount = new BigNumber(0);
        for (var i = 0; i < utxos.length; i++) {
            let utxo = utxos[i];
            if (utxo.amount <= 0) {
                continue;
            }
            sumAmount = sumAmount.plus(utxo.amount);
            availableUtxos.push({
                "txId": utxo.txid,
                "outputIndex": utxo.vout,
                "address": utxo.address,
                "script": utxo.scriptPubKey,
                "satoshis": Unit.fromBTC(utxo.amount).toSatoshis()
            });
        }
        rpcClient.omni_createpayload_simplesend(propertyId, amount, function (error, parsedBuf) {
            log(error, parsedBuf)
            if (parsedBuf["result"]) {
                var payload = parsedBuf["result"];
                let transaction = new bitcore.Transaction().fee(11000)
                    .from(availableUtxos)
                    .to(to, 546)
                    .change(availableUtxos[0].address)
                    .addData(Buffer.from("6f6d6e69" + payload, "hex"));

                pks.forEach(pk => {
                    transaction.sign(pk);
                });

                log(transaction.serialize())
                rpcClient.sendRawTransaction(transaction.serialize(), function (error, parsedBuf) {
                    log(error, parsedBuf)
                    fn && fn(parsedBuf["result"],amount,0.00011,availableUtxos,);
                });
            }
        });


    }
    async function waitResolve(methodName, ...args) {
        var result = undefined;
        var promise = new Promise((resolve, reject) => {
            var callback = function (error, parsedBuf) {
                if (error) {
                    console.error(methodName, error);
                }
                if (parsedBuf && parsedBuf["result"]) {
                    result = parsedBuf["result"]
                }
                resolve();
            }
            rpcClient[methodName](...args, callback);
        });
        await promise;
        return result;
    }
    this.getBlockNumber = async function () {
        return await waitResolve("getblockcount");
    }
    this.getBlock = async function (blockNumber) {
        var hash = await waitResolve("getblockhash", blockNumber);
        return await waitResolve("getblock", hash);
    }
    this.getTx = async function (txid) {
        const tx = await waitResolve("getrawtransaction", txid, 1);
        return tx;
    }
    this.listBlockTxs = async function (blockNumber) {
        return await waitResolve("omni_listblocktransactions", blockNumber);
    }
    this.getOmniTx = async function (txid) {
        return await waitResolve("omni_gettransaction", txid);
    }
    this.syncBlockChainTx = async function (bcSync, fn) {

        const headBlockNumber = await t.getBlockNumber();
        log("btc head blockNumber is _ " + headBlockNumber);
        var tx = undefined;
        while (bcSync.blockNumber <= headBlockNumber) {
            //查询交易信息
            var txids = await t.listBlockTxs(bcSync.blockNumber)
            if (txids && txids.length) {
                for (var j = 0; j < txids.length; j++) {
                    var tx = await t.getOmniTx(txids[j]);
                    if (tx.propertyid == propertyId) {//如果是usdt
                        await fn({ hash: tx.txid, index: -1, blockHash: "-", blockNumber: bcSync.blockNumber, fromAddr: tx.sendingaddress, toAddr: tx.referenceaddress, amount: tx.amount });
                    }
                }
            }

            bcSync.blockNumber++;
            log("omni bct block number " + bcSync.blockNumber)
        }
    }
    return this;
}
exports.CoinClient = CoinClient;