module.exports = {
  bitcoin: {
    messagePrefix: '\x18Bitcoin Signed Message:\n',
    bech32: 'btc',
    bip32: {
      public: 0x0488b21e,
      private: 0x0488ade4
    },
    pubKeyHash: 0x00,
    scriptHash: 0x05,
    wif: 0x80
  },
    bitcoinTest: {
    messagePrefix: '\x18Bitcoin Test Net Signed Message:\n',
    bech32: 'bitcoinTest',
    bip32: {
      public: 0x043587CF,
      private: 0x04358394
    },
    pubKeyHash: 0x6f,
    scriptHash: 0xc4,
    wif: 0xEF
  },
  litecoin:{
  messagePrefix: '\x19Litecoin Signed Message:\n',
  bip32: {
    public: 0x019da462,
    private: 0x019d9cfe
  },
  pubKeyHash: 0x30,
  scriptHash: 0x32,
  wif: 0xb0
  },
  litecoinRegTest:{
  messagePrefix: '\x19Litecoin RegTest Signed Message:\n',
  bip32: {
    public: 0x043587cf,
    private: 0x04358294
  },
  pubKeyHash: 0x6f,
  scriptHash: 0x3a,
  wif: 0xef
  },
  dogecoin:{
  messagePrefix: '\x19Dogecoin Main Net Signed Message:\n',
  bip32: {
    public: 0x02facafd,
    private: 0x02fac398
  },
  pubKeyHash: 0x1e,
  scriptHash: 0x16,
  wif: 0x9e
  },
  btgcoin:{
  messagePrefix: '\x1DBitcoin Gold Signed Message:\n',
    bech32: 'btg',
    bip32: {
      public: 0x0488b21e,
      private: 0x0488ade4
    },
    pubKeyHash: 0x26,
    scriptHash: 0x17,
    wif: 0x80,
    forkHeight: 491407,
    equihash: {
      n: 144,
      k: 5,
      person: 'BgoldPoW',
      equihashForkHeight: 536200,
      preEquihashFork: {
        n: 200,
        k: 9,
        person: 'ZcashPoW'
      }
    },
    lwma: {
      enableHeight: 536200,
      testnet: false,
      regtest: false,
      powTargetSpacing: 600,
      averagingWindow: 45,
      adjustWeight: 13772,
      minDenominator: 10,
      solveTimeLimitation: true,
      powLimit: '14134776517815698497336078495404605830980533548759267698564454644503805952'
    }
  },
  dashcoin:{
    messagePrefix: '\x19Dash Signed Message:\n',
    bip32: {
      private: 0x0488ade4,
      public: 0x0488b21e
    },
    pubKeyHash: 0x4c,
    scriptHash: 0x10,
    wif: 0xcc
  },
  qtumcoin:{
  messagePrefix: '\x19Qtum Main Net Signed Message:\n',
  bip32: {
    public: 0x0488b21e,
    private: 0x0488ade4
  },
  pubKeyHash: 0x3A,
  scriptHash: 0x32,
  wif: 0x80
  }
}