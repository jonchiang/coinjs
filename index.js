if (typeof log === 'undefined'){
	log = function(msg){
		console.log(msg)
	} 
}
function getDcId(symbol) {
    switch (symbol) {
        case 'btc':
            return 1;
        case 'eth':
            return 2;
        case 'eos':
            return 3;
        case 'ltc':
            return 4;
        case 'usdt':
            return 101;
    }
}
global.DFn = function(err){
    console.error(err);
}
module.exports.AddrDto = require("./coin/addrDto");
module.exports.btc = require("./coin/impl/btc");
module.exports.eth = require("./coin/impl/eth");
module.exports.eos = require("./coin/impl/eos");
module.exports.ethToken = require("./coin/impl/ethToken");
module.exports.usdt = require("./coin/impl/usdt");
module.exports.ltc = require("./coin/impl/ltc");
module.exports.bmj = require("./coin/impl/bmj");
module.exports.getDcId = getDcId;

